from __future__ import absolute_import
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'humanDepartment.settings')
app = Celery("humanDepartment")
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
response = app.control.enable_events(reply=True)

CELERY_BEAT_SCHEDULE = {
    'salary_payment_every_2_hours': {
        'task': 'employees.tasks.salary_payment',
        'schedule': crontab(minute=0, hour='*/2')
    }
}
