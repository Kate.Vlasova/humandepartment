from humanDepartment.celery import app
from celery import Celery
from celery.schedules import crontab
from .models import Employee
from django.db.models import F
from celery import shared_task

app = Celery()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(crontab(hour=2, minute=0), salary_payment, name='add every 2 hour')


@shared_task
def salary_payment():
    Employee.objects.all().bulk_update(Employee.objects.all().update(salary_paid=F("salary_paid") + F("salary_month")),
                                                                                                        ['salary_paid'])


@shared_task
def remove_paid_salary(employee_ids):
    Employee.objects.all().bulc_update(Employee.objects.filter(id__in=employee_ids).update(salary_paid=0),
                                                                                                        ['salary_paid'])
