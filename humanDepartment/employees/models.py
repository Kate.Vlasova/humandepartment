from django.db import models
from django.contrib.auth.models import AbstractUser


class Employee(AbstractUser):
    POSITION_DICT = {
        0: "SEO",
        1: "Vice President",
        2: "Senior Manager",
        3: "Manager",
        4: "Specialist"
    }

    full_name = models.CharField(max_length=100, blank=True, null=True)
    hire_date = models.DateField(blank=True, null=True)
    salary_month = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    salary_paid = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    manager = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)
    position = models.CharField(max_length=100, blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f"Employee: {self.full_name}"

    def __repr__(self):
        return self.__str__()

    def save(self, *args, **kwargs):
        """Automatic filling of the position and level of an employee."""
        if not self.manager:
            self.position = Employee.POSITION_DICT[0]
            self.level = 0
        for level in range(len(Employee.POSITION_DICT.keys())):
            if level == len(Employee.POSITION_DICT):
                raise Exception("Can't be a manager")
            elif self.manager in Employee.objects.filter(position=Employee.POSITION_DICT[level]):
                self.position = Employee.POSITION_DICT[level + 1]
                self.level = level + 1
        super().save(*args, **kwargs)
