from django.contrib import admin
from .models import Employee
from .forms import EmployeeForm
from django.urls import reverse
from django.utils.html import format_html
from .tasks import remove_paid_salary


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ("full_name", "position", "manager_link", "salary_month", "salary_paid")
    list_display_links = ["full_name"]
    list_filter = ("position", "level")
    actions = ["action_delete_salary_paid"]

    form = EmployeeForm

    def manager_link(self, obj):
        """Create a link to the manager."""
        if obj.manager:
            link = reverse("admin:employees_employee_change", args=[obj.manager_id])
            return format_html('<a href="{}">{}</a>', link, obj.manager)
        else:
            return format_html("<b><i>{}</i></b>", "Submits to himself")

    manager_link.short_description = 'Manager'

    def action_delete_salary_paid(self, request, queryset):
        """Delete information about salary paid."""
        if len(queryset) > 20:
            employee_ids = list(queryset.values_list('id', flat=True))
            remove_paid_salary.delay(employee_ids)
        else:
            queryset.update(salary_paid=0)


admin.site.register(Employee, EmployeeAdmin)
