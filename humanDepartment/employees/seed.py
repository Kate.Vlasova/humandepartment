import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'humanDepartment.settings')

import django

django.setup()

import random
from humanDepartment.employees.models import Employee
from django_seed import Seed

seeder = Seed.seeder()

seeder.add_entity(Employee, 100)

seeder.execute()
