from django.shortcuts import get_list_or_404
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from .models import Employee
from .serializers import EmployeeAllSerializer
from .permissions import EmployeeReadOnlyHimself


class EmployeeAllView(viewsets.ViewSet):
    permission_classes = (EmployeeReadOnlyHimself,)

    def get_list_all_employees(self, request):
        """Get all employees."""
        queryset = Employee.objects.all()
        serializer = EmployeeAllSerializer(queryset, many=True)
        return Response(serializer.data)

    def get_one_level_employees(self, request, level=None):
        """Get all employees of the same level from the list."""
        employees = Employee.objects.filter(level=level)
        users = get_list_or_404(employees)
        serializer = EmployeeAllSerializer(users, many=True)
        return Response(serializer.data)

    def get_one_employee(self, request, pk=None):
        """Get one employee from the list."""
        queryset = Employee.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        self.check_object_permissions(request, user)
        serializer = EmployeeAllSerializer(user)
        return Response(serializer.data)
