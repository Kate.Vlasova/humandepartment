from rest_framework import serializers
from .models import Employee


class EmployeeAllSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('full_name', 'hire_date', 'salary_month', 'salary_paid', 'manager', 'position', 'level')
