from django import forms
from .models import Employee


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = [
            "full_name",
            "username",
            "email",
            "password",
            "hire_date",
            "salary_month",
            "salary_paid",
            "manager"
        ]
