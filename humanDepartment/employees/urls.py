from django.urls import path
from .views import EmployeeAllView

urlpatterns = [
    path('employees/', EmployeeAllView.as_view({'get': 'get_list_all_employees'})),
    path('employees/<int:pk>', EmployeeAllView.as_view({'get': 'get_one_employee'})),
    path('employees/level/<int:level>', EmployeeAllView.as_view({'get': 'get_one_level_employees'}))

]
