#! / usr / bin / env python3

from humanDepartment.wsgi import *
from .models import Employee
import random, string, datetime


class AutoEmloyee(object):
    Id = 1

    def __init__(self):
        self.id = self.__class__.Id + 1 + max(Employee.objects.all().values_list('id', flat=True))
        self.username = ''.join(
            random.choice(string.ascii_lowercase.Alphabet) for i in range(random.randrange(5, 26, 1)))
        self.password = ''.join(
            random.choice(string.ascii_lowercase.Alphabet) for i in range(random.randrange(5, 26, 1)))
        self.full_name = ''.join(
            random.choice(string.ascii_lowercase.Alphabet) for i in range(random.randrange(5, 26, 1)))
        self.hire_date = datetime.datetime.now().date() - datetime.timedelta(random.randrange(0, 7500, 1)).strftime(
            "%Y-%m-%d")
        self.salary_month = random.randrange(1, 900000000, 1) / 100
        self.salary_paid = random.randrange(1, 900000000, 1) / 100
        self.__class__.Id += 1

        self.manager = self._check_manager()

        def _check_manager(self):
            list_id_managers = Employee.objects.filter(level__lte=4).values_list('id', flat=True)
            number = random.randrange(0, len(list_id_managers), 1)
            return list_id_managers[number]

        @classmethod
        def get_id(cls):
            return cls.Id

        def __str__(self):
            return self.username


quantityEmloyee = 100
listEmployees = []

for i in range(quantityEmloyee):
    listEmployees.append(AutoEmloyee())

Employee.objects.bulk_create(listEmployees)

