# Human Department 

The project is developed with the use of Django framework and designed as a catalog of employees with API interface and customized admin panel. 
JWT token authentication is implemented here.
____

Employee application allows to:
- receive a token: [http://localhost:8069/auth/jwt/create/](http://localhost:8069/auth/jwt/create/)
- get all employees of the department by the link: [http://localhost:8069/employees/](http://localhost:8069/employees/)
- get all employees of the company of the same level: [http://localhost:8069/employees/level/<int:level>/](http://localhost:8069/employees/level/<int:level>/)
- review an individual record of an employee: [http://localhost:8069/employees/<int:pk>/](http://localhost:8069/employees/<int:pk>/)
- use admin panel: [http://localhost:8069/admin/](http://localhost:8069/admin/)
____

The database can be filled in with the use of script and django-seed from the command line by using the following command:

```python manage.py seed Employees --number=100 ```

or by using the code demonstrated in the project.

## Built With :snake: 

  ```
  Django
  Django Rest Framework
  Postgresql
  Celery
  Celery-beat
  ```
  
## Getting Started :rocket: 
  
  To start with the project, one should set the initial dependencies:

  ```
  Python
  Docker
  ``` 
  
  Create .env file:

  ```python
  DJANGO_SECRET_KEY=""
  DB_ENGINE="django.db.backends.postgresql"
  DB_NAME="postgres"
  POSTGRES_USER="postgres"
  POSTGRES_PASSWORD="postgres"
  DB_HOST=db
  DB_PORT=5432
  ```
 
  Launch docker-compose by the command:

  ```docker-compose up --build```

 All migrations will be automatically loaded in the database.

## Contact

Vlasova Ekaterina: [ekaterina.d851@gmail.com](ekaterina.d851@gmail.com)

Telegram Link: [@katherine_vlasova](@katherine_vlasova)
